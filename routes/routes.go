package routes

import (
	handlers "login-page/handlers"

	"github.com/gin-gonic/gin"
)

func Routes() {
	r := gin.Default()
	r.POST("/user", handlers.CreateUser)
	r.GET("/user", handlers.GetUser)
	r.DELETE("/user", handlers.DeleteUser)
	r.PUT("/user", handlers.UpdatePassword)
	r.Run()
}
