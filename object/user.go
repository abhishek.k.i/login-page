package userobject

import (
	"github.com/gin-gonic/gin"
)

func Get(err error, c *gin.Context) {
	if err != nil {
		c.JSON(200, gin.H{
			"message": "user doesnot exist",
		})

	} else {

		c.JSON(200, gin.H{
			"message": "user verified",
		})
	}

}

func Create(err error, c *gin.Context) {
	if err != nil {
		c.JSON(200, gin.H{
			"message": "username already existed",
		})

	} else {

		c.JSON(200, gin.H{
			"message": "user registered",
		})
	}
}
func Delete(err error, c *gin.Context) {
	if err != nil {
		c.JSON(200, gin.H{
			"message": "username does not existed",
		})

	} else {

		c.JSON(200, gin.H{
			"message": "user deleted",
		})
	}

}
func Update(err error, c *gin.Context) {
	if err != nil {
		c.JSON(200, gin.H{
			"message": "username doesnot exist",
		})

	} else {

		c.JSON(200, gin.H{
			"message": "password updated",
		})
	}
}
