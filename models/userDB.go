package query

import (
	db "login-page/DB"
	user "login-page/entity"
	userreq "login-page/entity/request"

	_ "github.com/go-sql-driver/mysql"
)

var err error

func Get(NewUser user.User) error {
	err = db.Client.QueryRow("SELECT user_name FROM users WHERE user_name = ? AND password = ?", NewUser.UserName, NewUser.Password).Scan(&NewUser.UserName)
	return err
}

func Create(NewUser user.User) error {
	_, err = db.Client.Query("INSERT INTO users (user_name,password,email) VALUES(?,?,?)", NewUser.UserName, NewUser.Password, NewUser.Email)
	return err

}
func Delete(NewUser user.User) error {
	err = db.Client.QueryRow("SELECT user_name FROM users WHERE user_name = ? AND password = ?", NewUser.UserName, NewUser.Password).Scan(&NewUser.UserName)
	_ = db.Client.QueryRow("DELETE FROM users WHERE user_name = ? AND password= ?", NewUser.UserName, NewUser.Password)
	return err

}

func Update(NewUser userreq.User) error {

	err = db.Client.QueryRow("SELECT user_name FROM users WHERE user_name = ? AND password = ?", NewUser.UserName, NewUser.OldPassword).Scan(&NewUser.UserName)
	_ = db.Client.QueryRow("UPDATE users SET password = ? WHERE user_name = ? AND password = ?", NewUser.NewPassword, NewUser.UserName, NewUser.OldPassword)
	return err
}
