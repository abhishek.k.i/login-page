package db

import (
	"database/sql"

	_ "github.com/go-sql-driver/mysql"
)

var Client *sql.DB

func InitDb() {
	Client, _ = sql.Open("mysql", "root:1234@123@tcp(127.0.0.1:3306)/loginDB")
}
