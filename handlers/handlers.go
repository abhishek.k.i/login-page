package handlers

import (
	user "login-page/entity"
	userrequest "login-page/entity/request"
	userDB "login-page/models"
	userobject "login-page/object"

	"github.com/gin-gonic/gin"
)

var InputUser user.User
var UpdateInputUser userrequest.User

func GetUser(c *gin.Context) {

	c.ShouldBindJSON(&InputUser)
	err := userDB.Get(InputUser)
	userobject.Get(err, c)

}
func CreateUser(c *gin.Context) {
	c.ShouldBindJSON(&InputUser)
	err := userDB.Create(InputUser)
	userobject.Create(err, c)

}

func DeleteUser(c *gin.Context) {

	c.ShouldBindJSON(&InputUser)
	err := userDB.Delete(InputUser)
	userobject.Delete(err, c)
}

func UpdatePassword(c *gin.Context) {

	c.ShouldBindJSON(&UpdateInputUser)
	err := userDB.Update(UpdateInputUser)
	userobject.Update(err, c)

}
