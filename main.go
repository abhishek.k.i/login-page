package main

import (
	db "login-page/DB"
	"login-page/routes"
)

func main() {

	db.InitDb()
	routes.Routes()
}
